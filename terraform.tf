provider "aws" {
    region = "eu-central-1"
    profile = "int"
}

terraform {
  backend "s3" {
    bucket = "ar-remote-backend"
    key    = "WEB/SOLO/terraform.state"
    region = "eu-central-1"
    profile = "int"
    dynamodb_table = "solo-db"
  }
}

resource "aws_security_group" "instance_sg" {
    name = "terraform-test-sg"
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_autoscaling_group" "ag_test" {
  launch_configuration = aws_launch_configuration.lc_test.name
  vpc_zone_identifier  = data.aws_subnet_ids.default_subnets_ids.ids
  min_size = 2
  max_size = 10
  target_group_arns = [aws_lb_target_group.asg_target.arn]

  tag {
    key                 = "Name"
    value               = "terraform-asg-example"
    propagate_at_launch = true
  }
}

resource "aws_launch_configuration" "lc_test" {
  image_id        = "ami-0a49b025fffbbdac6"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.instance_sg.id]

  user_data = <<-EOF
                    #!/bin/bash
                    sudo apt-get update
                    sudo apt-get install -y apache2
                    sudo systemctl start apache2
                    sudo systemctl enable apache2
                    sudo echo "<h1>Hello devopssec</h1>" > /var/www/html/index.html
                EOF

    lifecycle {
    create_before_destroy = true
  }

}

data "aws_vpc" "default" { 
  default = true
}

data "aws_subnet_ids" "default_subnets_ids" {
  vpc_id = data.aws_vpc.default.id
}
/*data "aws_subnet_ids" "default_subnets_ids" {
  vpc_id = module.myvpc_module.id
}*/

#load balancer oblige 2 réseau subnet différents
#Possibilité de faire soit même le subnet

resource "aws_lb" "example_alb" {
  name               = "terraform-asg-example"
  load_balancer_type = "application"
  subnets            = data.aws_subnet_ids.default_subnets_ids.ids
  security_groups    = [aws_security_group.alb.id]
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.example_alb.arn
  port              = 80
  protocol          = "HTTP"

  # By default, return a simple 404 page
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code  = 404
    }
  }
}

resource "aws_security_group" "alb" {

  # Allow inbound HTTP requests
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound requests
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb_target_group" "asg_target" {

  port     = 80
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.default.id

  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
    interval            = 15
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }
}

resource "aws_lb_listener_rule" "asg_rule" {
  listener_arn = aws_lb_listener.http.arn
  priority     = 100

  condition {
    path_pattern {
      values = ["*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.asg_target.arn
  }
}

output "lb_dns" {
  value = aws_lb.example_alb.dns_name
}
 #creation VPC et subnet

 #TODO : variable
resource "aws_vpc" "the_vpc" {
  cidr_block = "10.0.0.0/16"
}

#faire une variameble list(map(string))
resource "aws_subnet" "main" {
  count=length(var.subnets)
  vpc_id     = aws_vpc.the_vpc.id
  cidr_block = var.subnets[count.index].cidr
  availability_zone = var.subnets[count.index].zone

  tags = {
    Name = "Main"
  }
}

/*variable "vpc_cidr" {
  type=string
}*/
variable "subnets"{
  type=list(map(string))
  default=[
    {
      "cidr":"10.0.1.0/24"
      "zone":"eu-central-1a"
    },
    {
      "cidr":"10.0.3.0/24"
      "zone":"eu-central-1c"
    },
    {
      "cidr":"10.0.2.0/24"
      "zone":"eu-central-1b"
    }
  ]
}
# Les variables du VPC peuvent être socké dans un dossier module de ce type : 
/*module "myvpc_module"{
  source="./module/vpc"
  vpc_cidr="10.0.0.0/16"
  subnets = [
    {
      "cidre":"10.0.1.0/24"
      "zone":"eu-central-1a"
    },
    {
      "cidre":"10.0.3.0/24"
      "zone":"eu-central-1c"
    },
    {
      "cidre":"10.0.2.0/24"
      "zone":"eu-central-1b"
    }
  ]
}*/